package api

import (
	"bufio"
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"net"
)

var addr *net.TCPAddr

func Initialize(address string) (err error) {
	addr, err = net.ResolveTCPAddr("tcp4", address)
	return
}

func NewPerformer(name, description, photo string) *PerformerEntity {
	return &PerformerEntity{Name: name, Description: description, Photo: photo}
}

func deserializePerformer(data []byte) (*PerformerEntity, error) {
	p := new(PerformerEntity)
	if err := json.Unmarshal(data, p); err != nil {
		return nil, err
	}

	return p, nil
}

func NewAlbum(name, date, photo string, performer *PerformerEntity) *AlbumEntity {
	return &AlbumEntity{Name: name, Date: date, Photo: photo, Performer: performer}
}

func deserializeAlbum(data []byte) (*AlbumEntity, error) {
	a := new(AlbumEntity)
	if err := json.Unmarshal(data, a); err != nil {
		return nil, err
	}

	return a, nil
}

func NewTrack(name, key string, performer *PerformerEntity, album *AlbumEntity) *TrackEntity {
	return &TrackEntity{Name: name, Key: key, Performer: performer, Album: album}
}

func deserializeTrack(data []byte) (*TrackEntity, error) {
	t := new(TrackEntity)
	if err := json.Unmarshal(data, t); err != nil {
		return nil, err
	}

	return t, nil
}

func NewGenre(name string) *GenreEntity {
	return &GenreEntity{Name: name}
}

func deserializeGenre(data []byte) (*GenreEntity, error) {
	g := new(GenreEntity)
	if err := json.Unmarshal(data, g); err != nil {
		return nil, err
	}

	return g, nil
}

func NewLyrics(track *TrackEntity, text string) *LyricsEntity {
	return &LyricsEntity{Track: track, Text: text}
}

func deserializeLyrics(data []byte) (*LyricsEntity, error) {
	l := new(LyricsEntity)
	if err := json.Unmarshal(data, l); err != nil {
		return nil, err
	}

	return l, nil
}

func deserializeLink(data []byte) (*linkEntity, error) {
	l := new(linkEntity)
	if err := json.Unmarshal(data, l); err != nil {
		return nil, err
	}

	return l, nil
}

func (performer *PerformerEntity) AddGenre(genre *GenreEntity) {
	performer.Genres = append(performer.Genres, genre)
}

func (performer *PerformerEntity) Write() (*PerformerEntity, error) {
	data, err := sendRequest(request{"/write/performer/", performer})
	if err != nil {
		return nil, err
	}

	return deserializePerformer(data)
}

func (album *AlbumEntity) SetLabel(label string) {
	album.Label = &label
}

func (album *AlbumEntity) AddGenre(genre *GenreEntity) {
	album.Genres = append(album.Genres, genre)
}

func (album *AlbumEntity) Write() (*AlbumEntity, error) {
	data, err := sendRequest(request{"/write/album/", album})
	if err != nil {
		return nil, err
	}

	return deserializeAlbum(data)
}

func (track *TrackEntity) AddGenre(genre *GenreEntity) {
	track.Genres = append(track.Genres, genre)
}

func (track *TrackEntity) AddLyrics(lyrics *LyricsEntity) {
	track.Lyrics = lyrics
}

func (track *TrackEntity) Write() (*TrackEntity, error) {
	data, err := sendRequest(request{"/write/track/", track})
	if err != nil {
		return nil, err
	}

	return deserializeTrack(data)
}

func (genre *GenreEntity) Write() (*GenreEntity, error) {
	data, err := sendRequest(request{"/write/genre/", genre})
	if err != nil {
		return nil, err
	}

	return deserializeGenre(data)
}

func (lyrics *LyricsEntity) Write() (*LyricsEntity, error) {
	data, err := sendRequest(request{"/write/lyrics/", lyrics})
	if err != nil {
		return nil, err
	}

	return deserializeLyrics(data)
}

func (lyrics *linkEntity) Write() (string, error) {
	data, err := sendRequest(request{"/write/link/", lyrics})
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func sendRequest(r request) ([]byte, error) {
	connection, err := net.DialTCP("tcp4", nil, addr)
	if err != nil {
		return nil, err
	}
	defer connection.Close()

	reader := bufio.NewReader(connection)
	writer := bufio.NewWriter(connection)

	data, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}

	if _, err := writer.Write(data); err != nil {
		return nil, err
	}

	if err := writer.WriteByte(0); err != nil {
		return nil, err
	}

	if err := writer.Flush(); err != nil {
		return nil, err
	}

	data, err = reader.ReadBytes(0)
	if err != nil {
		return nil, err
	}
	data = data[:len(data)-1]

	return data, nil
}

func GetPerformerById(id uint64) (*PerformerEntity, error) {
	data, err := sendRequest(request{"/get/performer/", PerformerEntity{Id: id}})
	if err != nil {
		return nil, err
	}

	return deserializePerformer(data)
}

func GetAlbumById(id uint64) (*AlbumEntity, error) {
	data, err := sendRequest(request{"/get/album/", AlbumEntity{Id: id}})
	if err != nil {
		return nil, err
	}

	return deserializeAlbum(data)
}

func GetTrackById(id uint64) (*TrackEntity, error) {
	data, err := sendRequest(request{"/get/track/", TrackEntity{Id: id}})
	if err != nil {
		return nil, err
	}

	return deserializeTrack(data)
}

func GetGenreById(id uint64) (*GenreEntity, error) {
	data, err := sendRequest(request{"/get/genre/", GenreEntity{Id: id}})
	if err != nil {
		return nil, err
	}

	return deserializeGenre(data)
}

func GetLyricsById(id uint64) (*LyricsEntity, error) {
	data, err := sendRequest(request{"/get/lyrics/", LyricsEntity{Id: id}})
	if err != nil {
		return nil, err
	}

	return deserializeLyrics(data)
}

func GetLinkByKey(key string) (string, error) {
	data, err := sendRequest(request{"/get/link/", linkEntity{Key: key}})
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func SearchPerformersByName(name string) ([]*PerformerEntity, error) {
	data, err := sendRequest(request{"/search/performer/", PerformerEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	p := make([]*PerformerEntity, 0)
	if err := json.Unmarshal(data, &p); err != nil {
		return nil, err
	}

	return p, nil
}

func SearchAlbumsByName(name string) ([]*AlbumEntity, error) {
	data, err := sendRequest(request{"/search/album/", AlbumEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	a := make([]*AlbumEntity, 0)
	if err := json.Unmarshal(data, &a); err != nil {
		return nil, err
	}

	return a, nil
}

func SearchTracksByName(name string) ([]*TrackEntity, error) {
	data, err := sendRequest(request{"/search/track/", TrackEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	t := make([]*TrackEntity, 0)
	if err := json.Unmarshal(data, &t); err != nil {
		return nil, err
	}

	return t, nil
}

func SearchAllByName(name string) ([]*PerformerEntity, []*AlbumEntity, []*TrackEntity, error) {
	data, err := sendRequest(request{"/search/all/", PerformerEntity{Name: name}})
	if err != nil {
		return nil, nil, nil, err
	}

	r := new(allResponse)
	if err := json.Unmarshal(data, r); err != nil {
		return nil, nil, nil, err
	}

	return r.Performers, r.Albums, r.Tracks, nil
}

func GetPerformersByGenre(genre *GenreEntity) ([]*PerformerEntity, error) {
	data, err := sendRequest(request{"/genre/performer/", genre})
	if err != nil {
		return nil, err
	}

	p := make([]*PerformerEntity, 0)
	if err := json.Unmarshal(data, &p); err != nil {
		return nil, err
	}

	return p, nil
}

func GetAlbumsByGenre(genre *GenreEntity) ([]*AlbumEntity, error) {
	data, err := sendRequest(request{"/genre/album/", genre})
	if err != nil {
		return nil, err
	}

	a := make([]*AlbumEntity, 0)
	if err := json.Unmarshal(data, &a); err != nil {
		return nil, err
	}

	return a, nil
}

func GetTrackByGenre(genre *GenreEntity) ([]*TrackEntity, error) {
	data, err := sendRequest(request{"/genre/track/", genre})
	if err != nil {
		return nil, err
	}

	t := make([]*TrackEntity, 0)
	if err := json.Unmarshal(data, &t); err != nil {
		return nil, err
	}

	return t, nil
}

func GetAllByGenre(genre *GenreEntity) ([]*PerformerEntity, []*AlbumEntity, []*TrackEntity, error) {
	data, err := sendRequest(request{"/genre/all/", genre})
	if err != nil {
		return nil, nil, nil, err
	}

	r := new(allResponse)
	if err := json.Unmarshal(data, r); err != nil {
		return nil, nil, nil, err
	}

	return r.Performers, r.Albums, r.Tracks, nil
}

func FindPerformersByName(name string) ([]*PerformerEntity, error) {
	data, err := sendRequest(request{"/find/performer/", PerformerEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	p := make([]*PerformerEntity, 0)
	if err := json.Unmarshal(data, &p); err != nil {
		return nil, err
	}

	return p, nil
}

func FindAlbumsByName(name string) ([]*AlbumEntity, error) {
	data, err := sendRequest(request{"/find/album/", AlbumEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	a := make([]*AlbumEntity, 0)
	if err := json.Unmarshal(data, &a); err != nil {
		return nil, err
	}

	return a, nil
}

func FindTracksByName(name string) ([]*TrackEntity, error) {
	data, err := sendRequest(request{"/find/track/", TrackEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	t := make([]*TrackEntity, 0)
	if err := json.Unmarshal(data, &t); err != nil {
		return nil, err
	}

	return t, nil
}

func FindGenreByName(name string) (*GenreEntity, error) {
	data, err := sendRequest(request{"/find/genre/", GenreEntity{Name: name}})
	if err != nil {
		return nil, err
	}

	return deserializeGenre(data)
}

func AllPerformers() ([]*PerformerEntity, error) {
	data, err := sendRequest(request{"/all/performer/", PerformerEntity{}})
	if err != nil {
		return nil, err
	}

	p := make([]*PerformerEntity, 0)
	if err := json.Unmarshal(data, &p); err != nil {
		return nil, err
	}

	return p, nil
}

func AllAlbums() ([]*AlbumEntity, error) {
	data, err := sendRequest(request{"/all/album/", AlbumEntity{}})
	if err != nil {
		return nil, err
	}

	a := make([]*AlbumEntity, 0)
	if err := json.Unmarshal(data, &a); err != nil {
		return nil, err
	}

	return a, nil
}

func AllTracks(name string) ([]*TrackEntity, error) {
	data, err := sendRequest(request{"/all/track/", TrackEntity{}})
	if err != nil {
		return nil, err
	}

	t := make([]*TrackEntity, 0)
	if err := json.Unmarshal(data, &t); err != nil {
		return nil, err
	}

	return t, nil
}

func AllGenres() ([]*GenreEntity, error) {
	data, err := sendRequest(request{"/all/genre/", GenreEntity{}})
	if err != nil {
		return nil, err
	}

	g := make([]*GenreEntity, 0)
	if err := json.Unmarshal(data, &g); err != nil {
		return nil, err
	}

	return g, nil
}

func WriteRawData(data []byte) (string, error) {
	key := fmt.Sprintf("%X", md5.Sum(data))

	l := linkEntity{key, data}
	link, err := l.Write()
	if len(link) == 0 {
		return "", errors.New("length of returned link is zero")
	}

	return key, err
}

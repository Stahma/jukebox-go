package api

type PerformerEntity struct {
	Id          uint64         `json:",omitempty"`
	Name        string         `json:",omitempty"`
	Description string         `json:",omitempty"`
	Photo       string         `json:",omitempty"`
	Albums      []*AlbumEntity `json:",omitempty"`
	Tracks      []*TrackEntity `json:",omitempty"`
	Genres      []*GenreEntity `json:",omitempty"`
}

type AlbumEntity struct {
	Id        uint64           `json:",omitempty"`
	Name      string           `json:",omitempty"`
	Date      string           `json:",omitempty"`
	Photo     string           `json:",omitempty"`
	Label     *string          `json:",omitempty"`
	Performer *PerformerEntity `json:",omitempty"`
	Tracks    []*TrackEntity   `json:",omitempty"`
	Genres    []*GenreEntity   `json:",omitempty"`
}

type TrackEntity struct {
	Id        uint64           `json:",omitempty"`
	Name      string           `json:",omitempty"`
	Key       string           `json:",omitempty"`
	Lyrics    *LyricsEntity    `json:",omitempty"`
	Performer *PerformerEntity `json:",omitempty"`
	Album     *AlbumEntity     `json:",omitempty"`
	Genres    []*GenreEntity   `json:",omitempty"`
}

type GenreEntity struct {
	Id         uint64             `json:",omitempty"`
	Name       string             `json:",omitempty"`
	Performers []*PerformerEntity `json:",omitempty"`
	Albums     []*AlbumEntity     `json:",omitempty"`
	Tracks     []*TrackEntity     `json:",omitempty"`
}

type LyricsEntity struct {
	Id    uint64       `json:",omitempty"`
	Track *TrackEntity `json:",omitempty"`
	Text  string       `json:",omitempty"`
}

type linkEntity struct {
	Key  string `json:",omitempty"`
	Data []byte `json:",omitempty"`
}

type request struct {
	Command string      `json:",omitempty"`
	Data    interface{} `json:",omitempty"`
}

type allResponse struct {
	Performers []*PerformerEntity `json:",omitempty"`
	Albums     []*AlbumEntity     `json:",omitempty"`
	Tracks     []*TrackEntity     `json:",omitempty"`
}
